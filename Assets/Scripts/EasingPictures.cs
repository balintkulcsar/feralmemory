﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EasingPictures : MonoBehaviour {

    public GameObject Animals;
    List<Transform> animalList = new List<Transform>();
    public float floatingSec;
    public float waitSec;
    List<Transform> shuffledAnimals;
    float startPosX = 5F;
    Transform animal;

    // Use this for initialization
    IEnumerator Start ()
    {
        InitAnimals();

        int animalcount = shuffledAnimals.Count;

        for (int i = 1; i <= animalcount - 1; i++)
        {
            animal = shuffledAnimals[i];
            animal.gameObject.SetActive(true);
 
            StartCoroutine(SmoothMove(new Vector2(5F, 0), new Vector2(0, 0), animal, floatingSec));
            yield return new WaitForSeconds(floatingSec + waitSec);
            StartCoroutine(SmoothMove(new Vector2(0, 0), new Vector2(-5F, 0), animal, floatingSec));
            yield return new WaitForSeconds(floatingSec);
        }  
    }

    private void InitAnimals()
    {
        Animals.SetActive(true);

        foreach (Transform trans in Animals.GetComponentsInChildren<Transform>())
        {
            trans.gameObject.SetActive(true);
            trans.position = new Vector2(startPosX, trans.position.y);
            animalList.Add(trans);
        }

        var rnd = new System.Random();
        shuffledAnimals = animalList.OrderBy(item => rnd.Next()).ToList<Transform>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private IEnumerator SmoothMove(Vector2 startPos, Vector2 endPos, Transform animal, float seconds)
    {
        float t = 0.0F;

        while (t <= 1.0)
        {
            t += Time.deltaTime / seconds;
            animal.position = Vector2.Lerp(startPos, endPos, Mathf.SmoothStep(0.0F, 1.0F, Mathf.SmoothStep(0.0F, 1.0F, t)));

            yield return null;
        }
    }
}
