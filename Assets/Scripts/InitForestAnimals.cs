﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitForestAnimals : MonoBehaviour {

    public GameObject Animals;
    public float FloatingSpeed;
    public int TimeInMiddle;
    List<Transform> animalList = new List<Transform>();
    Transform animal;
    float posX = 5F;
    int actualAnimal = 0;
    List<Transform> shuffledAnimals;
    Boolean noMoreAnimal = false;

    // Use this for initialization
    void Start ()
    {
        InitAnimals();

        var rnd = new System.Random();
        shuffledAnimals = animalList.OrderBy(item => rnd.Next()).ToList<Transform>();

        animal = shuffledAnimals[actualAnimal];
        animal.gameObject.SetActive(true);
    }

    private void InitAnimals()
    {
        Animals.SetActive(true);

        foreach (Transform trans in Animals.GetComponentsInChildren<Transform>())
        {
            trans.gameObject.SetActive(true);
            trans.position = new Vector2(posX, trans.position.y);
            animalList.Add(trans);
        }
        
    }

    private void ShowAnimals()
    {
        if (shuffledAnimals.Count >= actualAnimal)
        {
            if (animal.transform.position.x >= -5)
            {
                animal.transform.position = new Vector2(posX, animal.transform.position.y);
                posX -= 0.1F * FloatingSpeed;

                if (Math.Round(animal.transform.position.x, 1) == 0)
                {
                    Thread.Sleep(TimeInMiddle * 1000);
                }
            }
            else
            {
                if (actualAnimal == shuffledAnimals.Count - 1)
                {
                    Animals.SetActive(false);
                    noMoreAnimal = true;
                    return;
                }

                actualAnimal  += 1;
                posX = 5;
                animal = shuffledAnimals[actualAnimal];
 
                animal.transform.position = new Vector2(posX, animal.transform.position.y);
            }
        }
    }


    // Update is called once per frame
    void Update ()
    {
        if (!noMoreAnimal)
        {
            ShowAnimals();
        }
        
    }
}
